package b127.bolong.s03a1;

import java.util.Scanner;

public class Factorial {
    private static Scanner sc;
    public static void main (String[] args) {
        System.out.println("Factorial\n");


        // Activity:
        // Create a Java Program that accepts an integer and computes for
        // the factorial value and displays it to the console.

        int i = 1, Number;
        long Factorial = 1;
        sc = new Scanner(System.in);
        System.out.println(" Please Enter any number to Find Factorial: ");
        Number = sc.nextInt();

        while (i <= Number)  {
            Factorial = Factorial * i;
            i++;
        }
        System.out.format(" Factorial of %d = %d", Number, Factorial);
    }
}
